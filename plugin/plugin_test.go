package plugin

import (
	"os"
	"reflect"
	"testing"
)

func TestWalkForYaml(t *testing.T) {
	want := []string{
		"../test/fixtures/sample_app/simple_manifest.yml",
		"../test/fixtures/sample_app/yaml.yaml",
	}
	got := WalkForYaml("../test/fixtures/sample_app/")

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestMatch(t *testing.T) {
	var tcs = []struct {
		Path string
		Want bool
	}{
		{"../test/fixtures/sample_app/example.tpl", false},
		{"../test/fixtures/sample_app/not_a_manifest.yml", false},
		{"../test/fixtures/sample_app/simple_manifest.yml", true},
		{"../test/fixtures/sample_app/template.yml", false},
		{"../test/fixtures/sample_app/yaml.yaml", true},
		{"../test/fixtures/sample_app/subdirectory/not_a_manifest.yml", false},
	}

	for _, tc := range tcs {
		fi, err := os.Stat(tc.Path)
		if err != nil {
			t.Error(err)
			continue
		}
		got, err := Match(tc.Path, fi)
		if err != nil {
			t.Error(err)
			continue
		}
		if got != tc.Want {
			t.Errorf("Wrong result for %s: expecting %v but got %v", tc.Path, tc.Want, got)
		}
	}
}
