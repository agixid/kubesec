FROM golang:1.13 AS build
ENV CGO_ENABLED=0
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM alpine/helm:2.16.1
FROM kubesec/kubesec:57f15fc

ENV PATH="/home/app:${PATH}"
COPY --from=build /go/src/app/analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
